class MailsMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.mails_mailer.mails_created.subject
  #
  def mails_created
    attachments["Отчёт за #{Time.now.strftime("%d.%m.%Y")}"] = File.read("lib/#{Time.now.strftime("%d.%m.%Y")}.pdf")
    mail to: "rvv@openteam.ru",
    subject: "Отчёт за #{Time.now.strftime("%d.%m.%Y")}"
  end
end
