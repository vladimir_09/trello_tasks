set :output, "/path/to/my/cron_log.log"
set :output, {:error => "log/cron_error_log.log", :standard => "log/cron_log.log"}

# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
set :environment, "development"

every :day, at: '08:00 pm' do
    rake 'mails:send'
end