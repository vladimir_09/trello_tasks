class TrelloTasksPdf

  def initialize(data)
    @data = data
  end

  def render
    data = @data

    date = Time.now
    today = date.strftime('%d.%m.%Y')
    week_ago = (date - 7.days).strftime('%d.%m.%Y')

    Prawn::Document.generate("lib/#{date.strftime("%d.%m.%Y")}.pdf") do

      font("app/assets/fonts/DejaVuSans-Bold.ttf") do
        text "Отчёт", size: 18, align: :center
      end
      font("app/assets/fonts/DejaVuSans-Oblique.ttf") do
        text "О выполненных задачах за #{week_ago} - #{today}", size: 16, align: :center
      end
      text "\n\n"

      data.each do |item|
        font("app/assets/fonts/DejaVuSans-Oblique.ttf") do
          text "#{item[:members].uniq.join(", ")+', ' if item[:members].present?} #{item[:date] if item[:date].present?}", size: 10
        end
        font("app/assets/fonts/DejaVuSans-Bold.ttf") do
          text "#{item[:card_name] if item[:card_name].present?}"
        end
        font("app/assets/fonts/DejaVuSans.ttf") do
          text "#{item[:desc].gsub /^$\n/, '' if item[:desc].present?}"
        end
        font("app/assets/fonts/DejaVuSans-Bold.ttf") do
          text "#{item[:checklist_name]+':' if item[:checklist_name].present?}"
        end
        font("app/assets/fonts/DejaVuSans.ttf") do
          unless item[:checkitems] == ""  
            item[:checkitems].each do |checkitem|
              text "** #{checkitem}"
            end
          end
        end
        text "\n\n"
      end

    end
  end

end
