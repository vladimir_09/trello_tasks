require 'open-uri'

  class TrelloInfo
    attr_reader :keys, :board_id, :list_id
    
    def initialize
      @keys = "key=#{Settings['auth_data']['key']}&token=#{Settings['auth_data']['token']}"
      @board_id = Settings['trello']['board_id']  
      @list_id = Settings['trello']['list_id']
    end

    def main_url(api_link)
      url = "https://api.trello.com" + api_link + keys
      uri = URI.parse(url).read
      JSON.parse(uri)
    end

    def members
      {
        '515a9bbec2a8b81f6a0026c5' => 'Анна Скрипченко',
        '5d9193f39216ef841fa12545' => 'Алёна Тверскова', 
        '5a5c13a89ada0ed6d4e7fb39' => 'Виктория Мочалова', 
        '604b21726ec36f83b2b07c2c' => 'Владимир Рябухин', 
        '5dba66ffa557dd79b17bf3d1' => 'Гелани Гелисханов',
        '4f6000f38aad50f8437b3ce0' => 'Евгений Лапин',
        '60e27386fd525f87bd2e1b7d' => 'Иван Муленок',
        '56528026738b86d4b4f320a1' => 'Роман Назаров',
        '5f6d81fb1225996a15d71df4' => 'Сергей Леонов' 
      }
    end

    def board_lists
      board_list = main_url("/1/boards/#{board_id}/lists?").select { |bl| bl["name"] == "Готово" }
    end

    def cards_ids
      list_cards = main_url("/1/lists/#{board_lists[0]['id']}/cards?").map { |c| c['id'] }.first(10)
    end

    def cards
      cards_info = Array.new
      cards_ids.each do |id|
        users = Array.new
        card = main_url("/1/cards/#{id}?")
        if main_url("/1/cards/#{id}/actions?").present?
          date_last_activity = Time.parse(main_url("/1/cards/#{id}/actions?")[0]['date']) + 7.hours
        else
          date_last_activity = Time.parse(card['dateLastActivity']) + 7.hours
        end
        if Time.now() - 7.days <= date_last_activity
          users = main_url("/1/cards/#{id}/members?").map { |m| users.append(members[m['id']]) }
          checklist = main_url("/1/cards/#{id}/checklists?")[0]
          unless checklist == nil
            checkitems = checklist['checkItems'].map { |c| c['name'] }
          else  
            checklist = ''
            checkitems = ''
          end
          cards_info.append({members: users, 
                            date: date_last_activity.strftime("%d.%m.%Y в %H:%M:%S"),
                            card_name: card['name'],
                            desc: card['desc'],
                            checklist_name: checklist['name'], 
                            checkitems: checkitems})
        end
      end
      cards_info
    end

  end
