namespace :mails do

  desc 'mail sender'
  task send: :environment do
    TrelloTasksPdf.new(TrelloInfo.new.cards).render
    MailsMailer.mails_created.deliver_now
  end

end
