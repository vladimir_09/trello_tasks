# Preview all emails at http://localhost:3000/rails/mailers/mails_mailer
class MailsMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/mails_mailer/mails_created
  def mails_created
    MailsMailer.mails_created
  end

end
