require "test_helper"

class MailsMailerTest < ActionMailer::TestCase
  test "mails_created" do
    mail = MailsMailer.mails_created
    assert_equal "Mails created", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
